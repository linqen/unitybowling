﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour {
	public float timeToReset;
	float resetTime;
	Rigidbody rb;
	Vector3 startPosition;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.anyKeyDown) {
			rb.AddForce (new Vector3 (Random.Range (-70f, 70f), 0, 500));
		}
		if (resetTime < timeToReset) {
			resetTime += Time.deltaTime;
		} else {
			//UnityEngine.SceneManagement.SceneManager.LoadScene("Bowling");
			resetTime=0;
			rb.velocity=Vector3.zero;
			rb.angularVelocity = Vector3.zero;
			transform.position = startPosition;
		}
	}
}
