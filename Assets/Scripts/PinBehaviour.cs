﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinBehaviour : MonoBehaviour {
	Vector3 originalTransform;
	Quaternion originalRotation;
	Rigidbody rigidBody;

	bool firstCollision = false;

	void Start () {
		originalTransform = transform.position;
		originalRotation = transform.rotation;
		rigidBody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col){
		if (!firstCollision) {
			firstCollision = true;
			Invoke ("ToOrigin", 5);
		}
	}
	public void ToOrigin(){
		transform.position = originalTransform;
		transform.rotation = originalRotation;
		rigidBody.velocity = Vector3.zero;
		rigidBody.angularVelocity = Vector3.zero;
		firstCollision = false;
	}
}
